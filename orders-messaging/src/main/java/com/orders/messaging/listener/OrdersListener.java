package com.orders.messaging.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class OrdersListener {
    Logger logger = LoggerFactory.getLogger(OrdersListener.class);

    private List<String> logs = new ArrayList<>();

    @JmsListener(destination = "${orders.status.change.queue}")
    public void receiveMessage(String message) {
        logger.info(message);
        logs.add(message);
    }

    public List<String> getLogs(){
        return this.logs;
    }
}
