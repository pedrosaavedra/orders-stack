package com.orders.messaging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersMessagingApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersMessagingApplication.class, args);
	}

}
