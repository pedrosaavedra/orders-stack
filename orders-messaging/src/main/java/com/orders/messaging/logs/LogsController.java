package com.orders.messaging.logs;

import com.orders.messaging.listener.OrdersListener;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class LogsController {

    OrdersListener listener;

    @GetMapping("/logs")
    public List<String> getLogs() {
        return this.listener.getLogs();
    }
}
