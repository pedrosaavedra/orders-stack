package com.adm.orders.service;

import com.adm.orders.dto.RequestOrderDTO;
import com.adm.orders.dto.ResponseOrderDTO;
import com.adm.orders.entity.Order;
import com.adm.orders.enums.OrderStatus;
import com.adm.orders.repository.OrderRepository;
import com.adm.orders.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderServiceImpl orderService;


    @Test
    public void shouldFindOrderById() {
        when(this.orderRepository.findById(1L)).thenReturn(Optional.of(this.createOrder()));

        Optional<ResponseOrderDTO> orderDTO = this.orderService.findById(1L);
        orderDTO.ifPresent(dto -> {
            assertThat(dto.getId()).isEqualTo(1L);
            assertThat(dto.getPrice()).isEqualTo(10.0);
            assertThat(dto.getStatus()).isEqualTo(OrderStatus.NEW);
            assertThat(dto.getCreatedBy()).isEqualTo("Test_user");
        });

        verify(this.orderRepository, times(1)).findById(1L);
    }

    @Test
    public void shouldCreateAOrder() {
        when(this.orderRepository.save(any(Order.class))).thenReturn(this.createOrder());

        ResponseOrderDTO dto = this.orderService.addOrder(this.createOrderDto());
        assertThat(dto.getId()).isEqualTo(1L);
        assertThat(dto.getPrice()).isEqualTo(10.0);
        assertThat(dto.getStatus()).isEqualTo(OrderStatus.NEW);
        assertThat(dto.getCreatedBy()).isEqualTo("Test_user");

        verify(this.orderRepository, times(1)).save(any(Order.class));
    }

    @Test
    public void shouldDeleteOrder() {
        this.orderService.deleteOrder(1L);
        verify(this.orderRepository, times(1)).findById(1L);
    }

    @Test
    public void findAllOrders() throws Exception {
        when(this.orderRepository.findAll()).thenReturn(List.of(this.createOrder(), this.createOrder()));

        assertThat(this.orderRepository.findAll().size()).isEqualTo(2);

        verify(this.orderRepository, times(1)).findAll();
    }

    private RequestOrderDTO createOrderDto() {
        return RequestOrderDTO.builder()
                .id(1L)
                .status(OrderStatus.NEW.name())
                .price(BigDecimal.valueOf(50.0))
                .build();
    }

    private Order createOrder() {
        Order order = Order.newInstance(10.0, OrderStatus.NEW);
        order.setId(1L);
        order.setCreatedBy("Test_user");
        return order;
    }
}
