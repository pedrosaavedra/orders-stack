package com.adm.orders.repository;

import com.adm.orders.entity.Order;
import com.adm.orders.enums.OrderStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Import(AuditTestConfig.class)
@DataJpaTest
public class OrderRepositoryTest {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void shouldIncludeOneUser() {
        Order order = this.orderRepository.save(createOrder());
        assertThat(order.getPrice()).isEqualTo(10.0);
        assertThat(order.getStatus()).isEqualTo(OrderStatus.NEW);
        assertThat(order.getCreatedBy()).isEqualTo("Test_user");

    }

    @Test
    public void shouldUpdateUser() {
        Order order = this.orderRepository.save(createOrder());
        assertThat(order.getPrice()).isEqualTo(10.0);
        assertThat(order.getStatus()).isEqualTo(OrderStatus.NEW);
        assertThat(order.getCreatedBy()).isEqualTo("Test_user");

        order.setPrice(20.0);
        order.setStatus(OrderStatus.DELIVERED);
        this.orderRepository.save(order);

        this.orderRepository.findById(order.getId())
                .ifPresent(o -> {
                    assertThat(o.getPrice()).isEqualTo(20.0);
                    assertThat(o.getStatus()).isEqualTo(OrderStatus.DELIVERED);
                });
    }

    @Test
    public void shouldDeleteUser() {
        Order order = this.orderRepository.save(createOrder());
        assertThat(this.orderRepository.findById(order.getId())).isNotEmpty();
    }

    private Order createOrder() {
        Order order = Order.newInstance(10.0, OrderStatus.NEW);
        order.setCreatedBy("Test_user");
        order.setLastModifiedBy("Test_user");
        order.setCreatedDate(LocalDateTime.now());
        order.setLastModifiedDate(LocalDateTime.now());
        return order;
    }
}
