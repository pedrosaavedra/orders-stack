package com.adm.orders.repository;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.util.Optional;

@TestConfiguration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class AuditTestConfig {
    @Bean
    public AuditorAware<String> auditorProvider() {
        return () -> Optional.of("Test_user");
    }
}
