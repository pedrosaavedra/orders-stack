package com.adm.orders.controller;

import com.adm.orders.ManagementOrdersApplication;
import com.adm.orders.dto.RequestOrderDTO;
import com.adm.orders.dto.ResponseOrderDTO;
import com.adm.orders.enums.OrderStatus;
import com.adm.orders.repository.OrderRepository;
import com.adm.orders.service.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.headers.RequestHeadersSnippet;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = ManagementOrdersApplication.class)
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
public class OrderControllerTest {

    @Autowired
    private WebApplicationContext webapp;

    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private OrderService orderService;

    @MockBean
    private OrderRepository orderRepository;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .build();
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void shouldFindOrderById() throws Exception {
        when(this.orderService.findById(1L)).thenReturn(Optional.of(this.createResponseOrder()));

        mockMvc.perform(get("/orders/{id}", 1L)
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.price", is(50.0)))
                .andExpect(jsonPath("$.status", is("NEW")))
                .andExpect(jsonPath("$.createdBy", is("User_Test_Create")))
                .andExpect(jsonPath("$.createdDate").exists())
                .andExpect(jsonPath("$.lastModifiedBy", is("User_Test_Update")))
                .andExpect(jsonPath("$.lastModifiedDate").exists())
                .andDo(document("find-order-by-id",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        getRequestHeader(),
                        getResponseFields(false)));

        verify(this.orderService, times(1)).findById(1L);
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void whenItsAValidRequestThenShouldCreateAOrder() throws Exception {
        when(this.orderService.addOrder(any(RequestOrderDTO.class))).thenReturn(this.createResponseOrder());

        mockMvc.perform(post("/orders")
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .content("{\"price\":50.0,\"status\":\"NEW\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.price", is(50.0)))
                .andExpect(jsonPath("$.status", is("NEW")))
                .andExpect(jsonPath("$.createdBy", is("User_Test_Create")))
                .andExpect(jsonPath("$.createdDate").exists())
                .andExpect(jsonPath("$.lastModifiedBy", is("User_Test_Update")))
                .andExpect(jsonPath("$.lastModifiedDate").exists())
                .andDo(document("create-order",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        getRequestHeader(), getRequestFields(), getResponseFields(false)));

        verify(this.orderService, times(1)).addOrder(any(RequestOrderDTO.class));
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void whenItsAValidRequestThenShouldUpdateAOrder() throws Exception {
        when(this.orderService.updateOrder(eq(1L), any(RequestOrderDTO.class))).thenReturn(Optional.of(createResponseOrder()));

        mockMvc.perform(put("/orders/{id}", 1)
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .content("{\"price\":50.0,\"status\":\"NEW\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.price", is(50.0)))
                .andExpect(jsonPath("$.status", is("NEW")))
                .andExpect(jsonPath("$.createdBy", is("User_Test_Create")))
                .andExpect(jsonPath("$.createdDate").exists())
                .andExpect(jsonPath("$.lastModifiedBy", is("User_Test_Update")))
                .andExpect(jsonPath("$.lastModifiedDate").exists())
                .andDo(document("update-order",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        getRequestHeader(), getRequestFields(), getResponseFields(false)));

        verify(this.orderService, times(1)).updateOrder(eq(1L), any(RequestOrderDTO.class));
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void whenItsAValidRequestThenShouldDeleteAOrder() throws Exception {
        mockMvc.perform(delete("/orders/{id}", 1)
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andDo(document("delete-order",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        getRequestHeader()));

        verify(this.orderService, times(1)).deleteOrder(1L);

    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void whenItsAValidRequestThenShouldFindAllOrders() throws Exception {
        when(this.orderService.findAll()).thenReturn(List.of(this.createResponseOrder(), this.createResponseOrder()));

        mockMvc.perform(get("/orders")
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[1].id").exists())
                .andExpect(jsonPath("$[1].price", is(50.0)))
                .andExpect(jsonPath("$[1].status", is("NEW")))
                .andExpect(jsonPath("$[1].createdBy", is("User_Test_Create")))
                .andExpect(jsonPath("$[1].createdDate").exists())
                .andExpect(jsonPath("$[1].lastModifiedBy", is("User_Test_Update")))
                .andExpect(jsonPath("$[1].lastModifiedDate").exists())
                .andDo(document("list-all-orders",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        getRequestHeader(), getResponseFields(true)));

        verify(this.orderService, times(1)).findAll();
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void whenItsAValidRequestThenShouldFindAllOrdersPaginated() throws Exception {
        when(this.orderService.findPaginated(0, 10)).thenReturn(new PageImpl<>(List.of(this.createResponseOrder(), this.createResponseOrder())));

        mockMvc.perform(get("/orders")
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .param("page","0")
                .param("size","10")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.page", is(0)))
                .andExpect(jsonPath("$.size", is(2)))
                .andExpect(jsonPath("$.orders[0].id").exists())
                .andExpect(jsonPath("$.orders[0].price", is(50.0)))
                .andExpect(jsonPath("$.orders[0].status", is("NEW")))
                .andExpect(jsonPath("$.orders[0].createdBy", is("User_Test_Create")))
                .andExpect(jsonPath("$.orders[0].createdDate").exists())
                .andExpect(jsonPath("$.orders[0].lastModifiedBy", is("User_Test_Update")))
                .andExpect(jsonPath("$.orders[0].lastModifiedDate").exists())
                .andDo(document("list-all-orders-paginated",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        getRequestHeader(),
                        responseFields(
                                fieldWithPath("page").description("The current paginated page"),
                                fieldWithPath("size").description("The size of orders in the page"),
                                fieldWithPath("orders[].id").description("The id of the order"),
                                fieldWithPath("orders[].price").description("The total price of the order"),
                                fieldWithPath("orders[].status").description("The status of the order"),
                                fieldWithPath("orders[].createdBy").description("The user that created a order"),
                                fieldWithPath("orders[].createdDate").description("The creation date of the order"),
                                fieldWithPath("orders[].lastModifiedBy").description("The last user to modify a order"),
                                fieldWithPath("orders[].lastModifiedDate").description("The last date that a order was modified")
                        )
        ));

        verify(this.orderService, times(1)).findPaginated(0, 10);
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void whenStatusNotPresentThenNoOrderCreatedReturn400() throws Exception {
        mockMvc.perform(post("/orders")
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .content("{\"price\":50.0}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.error", is("The status can not be null")));

    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void whenInvalidStatusThenNoOrderCreatedReturn400() throws Exception {
        mockMvc.perform(post("/orders")
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .content("{\"id\":1,\"price\":50.0,\"status\":\"N\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andExpect(jsonPath("$.error").exists())
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.error", is("Status must match NEW|PROCESSING|CANCELED|DELIVERED")));
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void whenPriceNotPresentThenNoOrderCreatedReturn400() throws Exception {
        var order = RequestOrderDTO.builder()
                .id(1L)
                .status(OrderStatus.NEW.name())
                .build();

        String body = objectMapper.writeValueAsString(order);

        mockMvc.perform(post("/orders")
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.error", is("The price can not be null")));

    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void whenPageParameterIsNotValidThenShouldReturn400() throws Exception {

        mockMvc.perform(get("/orders")
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .param("page", "p")
                .param("size", "10")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is("p is not a valid value for parameter page")));

    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void whenPahParameterIdIsNotValidThenShouldNotDeleteReturn400() throws Exception {
        mockMvc.perform(delete("/orders/{id}", "p")
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is("p is not a valid value for parameter id")));
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void whenPahParameterIdIsNotValidThenShouldNotUpdateReturn400() throws Exception {
        mockMvc.perform(put("/orders/{id}", "p")
                .header("Authorization", "Bearer {azureAdAccessToken}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is("p is not a valid value for parameter id")));
    }

    private ResponseOrderDTO createResponseOrder() {
        return ResponseOrderDTO.builder()
                .id(1L)
                .status(OrderStatus.NEW)
                .createdBy("User_Test_Create")
                .createdDate(LocalDateTime.now())
                .lastModifiedBy("User_Test_Update")
                .lastModifiedDate(LocalDateTime.now())
                .price(50.0)
                .build();
    }

    public RequestHeadersSnippet getRequestHeader() {
        return requestHeaders(headerWithName("Authorization").description("Azure ad token"));
    }

    private RequestFieldsSnippet getRequestFields() {
        ConstraintDescriptions constraintDescriptions = new ConstraintDescriptions(RequestOrderDTO.class);
        return requestFields(
                fieldWithPath("price").description("The total price of the order")
                        .attributes(key("constraints").value(constraintDescriptions.descriptionsForProperty("price"))),
                fieldWithPath("status").description("The status of the order")
                        .attributes(key("constraints").value(constraintDescriptions.descriptionsForProperty("status"))));
    }

    private ResponseFieldsSnippet getResponseFields(boolean isList) {
        if (isList) {
            return responseFields(fieldWithPath("[].id").description("The id of the order"),
                    fieldWithPath("[].price").description("The total price of the order"),
                    fieldWithPath("[].status").description("The status of the order"),
                    fieldWithPath("[].createdBy").description("The user that created a order"),
                    fieldWithPath("[].createdDate").description("The creation date of the order"),
                    fieldWithPath("[].lastModifiedBy").description("The last user to modify a order"),
                    fieldWithPath("[].lastModifiedDate").description("The last date that a order was modified")
            );
        }
        return responseFields(fieldWithPath("id").description("The id of the order"),
                fieldWithPath("price").description("The total price of the order"),
                fieldWithPath("status").description("The status of the order"),
                fieldWithPath("createdBy").description("The user that created a order"),
                fieldWithPath("createdDate").description("The creation date of the order"),
                fieldWithPath("lastModifiedBy").description("The last user to modify a order"),
                fieldWithPath("lastModifiedDate").description("The last date that a order was modified")
        );
    }

}

