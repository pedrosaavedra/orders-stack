= Orders Service API
:toc: left

== Introduction

In order to run the Orders Service CRUD you need an access token __(see README file for generation)__.
After generating the token you can replace every __\{azureAdAccessToken\}__ for it. If you prefer
to test it with PostMan there is a file __(Orders.postman_collection.json)__ in the project root folder. Eventually you will need to change the token.

== Resources

=== Create Order

.request fields
include::{snippets}/create-order/request-fields.adoc[]

.curl request
include::{snippets}/create-order/curl-request.adoc[]

.response
include::{snippets}/create-order/http-response.adoc[]

.response fields
include::{snippets}/create-order/response-fields.adoc[]

=== List Orders

.curl request
include::{snippets}/list-all-orders/curl-request.adoc[]

.response
include::{snippets}/list-all-orders/http-response.adoc[]

.response fields
include::{snippets}/list-all-orders/response-fields.adoc[]

=== List Orders Paginated

.curl request
include::{snippets}/list-all-orders-paginated/curl-request.adoc[]

.response
include::{snippets}/list-all-orders-paginated/http-response.adoc[]

.response fields
include::{snippets}/list-all-orders-paginated/response-fields.adoc[]

=== Find Order

.curl request
include::{snippets}/find-order-by-id/curl-request.adoc[]

.response
include::{snippets}/find-order-by-id/http-response.adoc[]

.response fields
include::{snippets}/find-order-by-id/response-fields.adoc[]

=== Update Order

.curl request
include::{snippets}/update-order/curl-request.adoc[]

.response
include::{snippets}/update-order/http-response.adoc[]

.response fields
include::{snippets}/update-order/response-fields.adoc[]

=== Delete Order

.curl request
include::{snippets}/delete-order/curl-request.adoc[]

.response
include::{snippets}/delete-order/http-response.adoc[]