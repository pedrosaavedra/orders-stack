package com.adm.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

//@EnableCircuitBreaker
@SpringBootApplication
public class ManagementOrdersApplication {
    public static void main(String[] args) {
        SpringApplication.run(ManagementOrdersApplication.class, args);
    }
}
