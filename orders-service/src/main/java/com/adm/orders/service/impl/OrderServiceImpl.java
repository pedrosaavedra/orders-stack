package com.adm.orders.service.impl;

import com.adm.orders.dto.RequestOrderDTO;
import com.adm.orders.dto.ResponseOrderDTO;
import com.adm.orders.entity.Order;
import com.adm.orders.publisher.OrderMessagePublisher;
import com.adm.orders.repository.OrderRepository;
import com.adm.orders.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private OrderMessagePublisher publisher;
    private OrderRepository orderRepository;

    @Override
    public ResponseOrderDTO addOrder(RequestOrderDTO dto) {
        return toOrderDTO(this.orderRepository.save(toOrder(dto)));
    }

    @Override
    public Page<ResponseOrderDTO> findPaginated(int page, int size) {
        return orderRepository.findAll(PageRequest.of(page, size)).map(this::toOrderDTO);
    }

    @Override
    public Optional<ResponseOrderDTO> updateOrder(Long id, RequestOrderDTO dto) {
        return this.orderRepository.findById(id)
                .map(order -> {
                    var oldStatus = order.getStatus();
                    order.setPrice(dto.getPrice().doubleValue());
                    order.setStatus(dto.getStatus());
                    var nDto = toOrderDTO(this.orderRepository.save(order));

                    publisher.send(String.format("User %s change order %s status from %s to %s",
                            order.getLastModifiedBy(),
                            id,
                            oldStatus,
                            nDto.getStatus().name()));

                    return nDto;
                });
    }

    @Override
    public void deleteOrder(Long id) {
        this.orderRepository.findById(id)
                .ifPresent(this.orderRepository::delete);
    }

    @Override
    public List<ResponseOrderDTO> findAll() {
        return this.orderRepository.findAll()
                .stream().map(this::toOrderDTO)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<ResponseOrderDTO> findById(Long id) {
        return this.orderRepository.findById(id).map(this::toOrderDTO);
    }

    private ResponseOrderDTO toOrderDTO(Order order) {
        return ResponseOrderDTO.builder()
                .id(order.getId())
                .price(order.getPrice())
                .status(order.getStatus())
                .createdBy(order.getCreatedBy())
                .createdDate(order.getCreatedDate())
                .lastModifiedBy(order.getLastModifiedBy())
                .lastModifiedDate(order.getLastModifiedDate())
                .build();
    }

    private Order toOrder(RequestOrderDTO dto) {
        return Order.newInstance(dto.getPrice().doubleValue(), dto.getStatus());
    }
}
