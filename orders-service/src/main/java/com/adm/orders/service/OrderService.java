package com.adm.orders.service;

import com.adm.orders.dto.RequestOrderDTO;
import com.adm.orders.dto.ResponseOrderDTO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    ResponseOrderDTO addOrder(RequestOrderDTO dto);

    Page<ResponseOrderDTO> findPaginated(int page, int size);

    Optional<ResponseOrderDTO> updateOrder(Long id, RequestOrderDTO dto);

    void deleteOrder(Long id);

    List<ResponseOrderDTO> findAll();

    Optional<ResponseOrderDTO> findById(Long id);
}
