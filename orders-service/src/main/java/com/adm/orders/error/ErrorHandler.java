package com.adm.orders.error;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.time.LocalDateTime;

import static java.lang.String.format;

@ControllerAdvice
public class ErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handle(MethodArgumentNotValidException exception) throws IOException {
        var msg = exception.getBindingResult().getFieldErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .findFirst()
                .orElse(exception.getMessage());
        return getErrorResponseEntity(msg);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handle(MethodArgumentTypeMismatchException exception, HttpServletResponse response) throws IOException {
        return getErrorResponseEntity(format("%s is not a valid value for parameter %s", exception.getValue(), exception.getName()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handle(ConstraintViolationException exception) {
        var msg = exception.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .findFirst().orElseGet(exception::getMessage);
        return getErrorResponseEntity(msg);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handle(Exception exception, WebRequest request) {
        return getErrorResponseEntity(exception.getMessage());
    }

    private ResponseEntity<Object> getErrorResponseEntity(String msg) {
        CustomErrorResponse errors = CustomErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .error(msg)
                .status(HttpStatus.BAD_REQUEST.value()).build();
        return ResponseEntity.badRequest().body(errors);
    }
}
