package com.adm.orders.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

import java.util.HashSet;

import static java.lang.String.format;

public enum OrderStatus {
    NEW(0),
    PROCESSING(1),
    DELIVERED(2),
    CANCELED(3);

    private int code;

    OrderStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    @JsonCreator
    public static OrderStatus fromString(String value) {
        try {
            return OrderStatus.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException ex) {
            throw new ValidationException("Unique Email Violation");
//            throw new RuntimeException(format("%s is not a valid value for status (NEW|PROCESSING|DELIVERED|CANCELED)", value));
        }
    }
}
