package com.adm.orders.entity;

import com.adm.orders.enums.OrderStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "ORDERS")
@RequiredArgsConstructor
public class Order extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double price;

    @Enumerated(EnumType.ORDINAL)
    private OrderStatus status;

    private Order(Double price, OrderStatus status) {
        this.price = price;
        this.status = status;
    }

    public static Order newInstance(Double price, OrderStatus status) {
        return new Order(price, status);
    }
}

