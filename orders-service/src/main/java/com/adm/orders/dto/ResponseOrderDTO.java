package com.adm.orders.dto;

import com.adm.orders.enums.OrderStatus;
import lombok.Builder;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Builder
public class ResponseOrderDTO {
    private Long id;
    private Double price;
    private OrderStatus status;

    protected String createdBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime createdDate;

    protected String lastModifiedBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime lastModifiedDate;
}
