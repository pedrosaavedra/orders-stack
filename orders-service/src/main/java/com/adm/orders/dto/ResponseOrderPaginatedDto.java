package com.adm.orders.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class ResponseOrderPaginatedDto {
    private int page;
    private int size;
    List<ResponseOrderDTO> orders;
}
