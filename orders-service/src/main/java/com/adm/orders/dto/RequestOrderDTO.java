package com.adm.orders.dto;

import com.adm.orders.enums.OrderStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Getter
@Builder
public class RequestOrderDTO {
    private Long id;

    @NotNull(message = "The price can not be null")
    @DecimalMin("0.00")
    private BigDecimal price;

    @Pattern(regexp="NEW|PROCESSING|CANCELED|DELIVERED", message = "Status must match NEW|PROCESSING|CANCELED|DELIVERED")
    @NotNull(message = "The status can not be null")
    private String status;

    public OrderStatus getStatus() {
        return OrderStatus.fromString(this.status);
    }
}
