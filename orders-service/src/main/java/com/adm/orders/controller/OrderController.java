package com.adm.orders.controller;

import com.adm.orders.dto.RequestOrderDTO;
import com.adm.orders.dto.ResponseOrderDTO;
import com.adm.orders.dto.ResponseOrderPaginatedDto;
import com.adm.orders.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;

@Validated
@RestController
@RequestMapping(
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class OrderController {

    private OrderService orderService;

    @GetMapping(value = "/orders")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<List<ResponseOrderDTO>> findAll() {
        return ResponseEntity.ok(this.orderService.findAll());
    }

    @GetMapping(value = "/orders", params = {"page", "size"})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseOrderPaginatedDto findAllPaginated(@RequestParam("page") @NotNull @Min(0) Integer page,
                                                      @RequestParam("size") @NotNull @Min(1) Integer size) {
        Page<ResponseOrderDTO> paginated = this.orderService.findPaginated(page, size);
        return ResponseOrderPaginatedDto.builder()
                .size(paginated.getSize())
                .page(page)
                .orders(paginated.toList())
                .build();
    }

    @GetMapping(value = "/orders/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<ResponseOrderDTO> findById(@PathVariable("id") @NotNull @Min(0) Long id) {
        return this.orderService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(value = "/orders")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<ResponseOrderDTO> createOrder(@Valid @RequestBody RequestOrderDTO order) {
        ResponseOrderDTO newOrder = this.orderService.addOrder(order);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(newOrder.getId()).toUri();

        return ResponseEntity.created(location).body(newOrder);
    }

    @PutMapping(value = "/orders/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<ResponseOrderDTO> updateOrder(@PathVariable @NotNull @Min(0) Long id,
                                                        @Valid @RequestBody RequestOrderDTO order) {
        return this.orderService.updateOrder(id, order)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping(value = "/orders/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<HttpStatus> deleteOrder(@PathVariable @NotNull @Min(0) Long id) {
        this.orderService.deleteOrder(id);
        return ResponseEntity.noContent().build();

    }
}
