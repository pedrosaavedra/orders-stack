package com.adm.orders.publisher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class OrderMessagePublisher {

    Logger logger = LoggerFactory.getLogger(OrderMessagePublisher.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    @Value("${orders.status.change.queue}")
    private String ordersStatusChangeQueue;

    public void send(String message) {

        CircuitBreaker circuitBreaker = circuitBreakerFactory.create("orders-circuit-breaker");

        var result = circuitBreaker.run(() -> {
                    jmsTemplate.convertAndSend(ordersStatusChangeQueue, message);
                    return message;
                },
                throwable -> "Error trying to connect to orders queue we could use prometheus to send alerts.");
        logger.info(result);
    }
}
