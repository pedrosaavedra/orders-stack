# Orders Stack 

This stack consists in  2 spring projects and one ActiveMQ:

- Orders Service 
    * A service with a CRUD for orders, and a queue publisher when updating an order
- ActiveMQ
    * An artemis docker image holding a orders-queue
- Orders Messaging Service
    * A service that listens from ActiveMQ orders-queue, it logs and saves the message in a list every time that Orders Service posts a message.
    

# Running the stack

### Running  ActiveMQ

- In order to create the Queue it is necessary to run the command: 
```shell
docker run -it --rm -p 8161:8161 -p 61616:61616 -e ARTEMIS_USERNAME=admin -e ARTEMIS_PASSWORD=pass vromero/activemq-artemis
```
It is possible to check the messages in the home broker in http://localhost:8161

### Running Orders Service

- In the project root folder (orders-service) runs the commands:
```shell
    mvn clean package
    mvn spring-boot:run
```
- It is possible to check the API documentation in http://localhost:8080/docs/api-orders.html
- To run the CRUD operations you need to generate a token from Azure AD running the command:
```shell
curl --location --request POST 'https://login.microsoftonline.com/6e146104-b7cc-435b-8243-8498f07f99f2/oauth2/v2.0/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'client_id=e0d11e61-83a7-4573-bfd2-8428f0215cee' \
--data-urlencode 'username=orders@pedrosaavedraaoutlook.onmicrosoft.com' \
--data-urlencode 'password=Opass#@!' \
--data-urlencode 'scope=api://e0d11e61-83a7-4573-bfd2-8428f0215cee/orders.all' \
--data-urlencode 'client_secret=~bG6qhr5_0yB1-L-0mQ9OGV~T0jhcADdeX'
```
- If you prefer to test on Postman you can import the file Orders.postman_collection.json that is in the root folder,
  there is a call to generate the token then you can replace it in the Postman variable that holds the token.
  
### Running Orders Messaging Service

- In the project root folder (orders-messaging) runs the commands:
```shell
    mvn clean package
    mvn spring-boot:run
```

It is possible to check the messages logs in the URL http://localhost:8081/logs

or running the command:

```shell
curl --location --request GET 'localhost:8081/logs'
```

### Circuit Breaker

In order to test the circuit breaker you need to find the artemis running container kill it and try to update an Order, then the circuit opened, and a log is shown in the console.